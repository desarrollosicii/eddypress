<?php get_header(); ?>
<!-- Comienza el cuerpo -->
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
                    
                <div class="blog-post">
                    <h2 id="post-<?php the_ID(); ?>" class="blog-post-title">
                        <a href="<?php the_permalink() ?>">
                            <?php the_title(); ?>
                        </a>
                    </h2>
                    <p class="blog-post-meta"><?php the_date(); ?>, <?php the_time(); ?> by <a href="#">Mark</a></p>

                    <?php the_content('<p class="serif">Leer el resto de esta página &raquo;</p>'); ?>


                    <?php edit_post_link('Editar esta entrada.', '<p>', '</p>'); ?>
                </div>
            <?php endwhile; ?>
                <nav>
                    <ul class="pager">
                      <li><a href="#">Previous</a></li>
                      <li><a href="#">Next</a></li>
                    </ul>
                </nav>

        <?php else : ?>

                <div class="blog-post">
                    <h2 class="blog-post-title">Lo sentimos, no se encontro</h2>

                    <p>Algo que estas buscando en esta sección, no existe.</p>


                </div>

        <?php endif; ?>


<?php get_footer(); ?>