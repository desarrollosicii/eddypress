<!DOCTYPE html>
<html lang="<?php bloginfo('language'); ?>">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Mi primera plantilla wordpress</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://getbootstrap.com/examples/blog/blog.css" rel="stylesheet">
</head>
<body>

    <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item active" href="<?php bloginfo('home'); ?>">Home</a>
          <a class="blog-nav-item" href="#">New features</a>
          <a class="blog-nav-item" href="#">Press</a>
          <a class="blog-nav-item" href="#">New hires</a>
          <a class="blog-nav-item" href="#">About</a>
        </nav>
      </div>
    </div>

    <div class="container">
        <div class="blog-header">
            <h1 class="blog-title">
            	<a href="<?php bloginfo('home'); ?>"><?php bloginfo('name'); ?></a>
            </h1>
            <p class="lead blog-description"><?php bloginfo('description'); ?></p>
        </div>
        <div class="row">
            <div class="col-sm-8 blog-main">

            <!-- Termina la cabeza -->