<?php get_header(); ?>
<!-- Comienza el cuerpo -->
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
                    
                <div class="blog-post">
                    <h2 id="post-<?php the_ID(); ?>" class="blog-post-title">
                        <?php the_title(); ?>
                    </h2>
                    <p class="blog-post-meta"><?php the_date(); ?> by <?php the_author(); ?></p>
                    <?php edit_post_link('Editar esta entrada.', '<p>', '</p>'); ?>

                    <?php the_content('<p class="serif">Leer el resto de esta página &raquo;</p>'); ?>


                </div>
            <?php endwhile; ?>
                <nav>
                    <ul class="pager">
                      <li><?php previous_post_link(); ?></li>
                      <li><?php next_post_link(); ?></li>
                    </ul>
                </nav>

        <?php else : ?>

                <div class="blog-post">
                    <h2 class="blog-post-title">Lo sentimos, no se encontro</h2>

                    <p>Algo que estas buscando en esta sección, no existe.</p>


                </div>

        <?php endif; ?>


<?php get_footer(); ?>